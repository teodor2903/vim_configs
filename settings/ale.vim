let g:ale_linters = {
      \  'typescript': [ 'tslint'],
      \  'ruby': [ 'rubocop'],
      \  'go': [ 'gometalinter']
      \}
let g:ale_fixers = {
      \  'typescript': [ 'tslint'],
      \  'ruby': [ 'rubocop']
      \}
let g:ale_sign_error = '❌'
let g:ale_sign_warning = '⚠️'
