" work with buffers
map  <S-Left> :bp<cr>
nmap <S-Left> :bp<cr>
imap <S-Left> <ESC>:bp<cr>i
map  <S-Right> :bn<cr>
nmap <S-Right> :bn<cr>
imap <S-Right> <ESC>:bn<cr>i

nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-l> :wincmd l<CR>
map <leader>g :!ruby %<cr>
map <leader>z :!node %<cr>

" Resize windows with arrow keys
nnoremap <C-Up> :resize +10<CR>
nnoremap <C-Down> :resize -10<CR>
nnoremap <C-Left> :vertical resize -30<CR>
nnoremap <C-Right> :vertical resize +30<CR>
