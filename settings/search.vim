" improve search
set incsearch
set ignorecase
set smartcase

let s:os = system("uname")
if s:os =~ "Darwin"
    let g:Grep_Xargs_Options='-0'
  endif

" grep
let Grep_Skip_Dirs = 'log tmp .git vendor bower_components node_modules coverage'
nnoremap <silent> <F5> :Rgrep<cr>

" CtrlP ignore
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/vendor/*,*/\.git/*,*/node_modules/*,*/bower_components/*,*/coverage/*
