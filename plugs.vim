call plug#begin('~/.vim/plugged')

" ruby rails plugins
Plug 'tpope/vim-rails'
Plug 'tpope/vim-rake',     { 'for': 'ruby' }
Plug 'tpope/vim-rvm',      { 'for': 'ruby' }
Plug 'vim-ruby/vim-ruby',  { 'for': 'ruby' }
Plug 'gabebw/vim-spec-runner', { 'for': 'ruby' }
Plug 'ngmy/vim-rubocop',   { 'for': 'ruby' }

Plug 'tpope/vim-endwise'
Plug 'jgdavey/vim-blockle'
Plug 'tpope/vim-surround'
Plug 'tomtom/tcomment_vim'
Plug 'Raimondi/delimitMate'

Plug 'sheerun/vim-polyglot'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'garbas/vim-snipmate'
Plug 'honza/vim-snippets'

Plug 'mattn/emmet-vim'
Plug 'gregsexton/MatchTag'
Plug 'szw/vim-tags'
Plug 'gcorne/vim-sass-lint'
Plug 'w0rp/ale'
Plug 'kien/ctrlp.vim'
Plug 'jpo/vim-railscasts-theme'
Plug 'nightsense/seagrey'

" Nerd tree file manager
Plug 'jistr/vim-nerdtree-tabs', { 'on':  'NERDTreeToggle' }
Plug 'scrooloose/nerdtree',     { 'on':  'NERDTreeToggle' }

" plugins
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'

Plug 'yegappan/grep'

Plug 'burnettk/vim-angular'
Plug 'leafgarland/typescript-vim'
Plug 'Quramy/tsuquyomi'
Plug 'bdauria/angular-cli.vim'

Plug 'posva/vim-vue'

Plug 'Shougo/vimproc.vim',     { 'do': 'make' }
Plug 'Shougo/vimshell.vim'

Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'sebdah/vim-delve'

Plug 'Valloric/YouCompleteMe', { 'do': './install.py'}

" --- Python ---
Plug 'klen/python-mode', { 'for': 'python' }	        " Python mode (docs, refactor, lints, highlighting, run and ipdb and more)
Plug 'davidhalter/jedi-vim', { 'for': 'python' } 		" Jedi-vim autocomplete plugin
Plug 'mitsuhiko/vim-jinja', { 'for': 'python' }		" Jinja support for vim
Plug 'mitsuhiko/vim-python-combined', { 'for': 'python' }  " Combined Python 2/3 for Vim


call plug#end()
