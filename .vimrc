set encoding=utf-8
set nocompatible

" Plugin manager
if filereadable(expand("~/.vim/plugs.vim"))
  source ~/.vim/plugs.vim
endif

" Custom settings
set backspace=indent,eol,start
set history=1000
set gcr=a:blinkon0
set autoread
set hidden
set number
let mapleader=","
set timeoutlen=1000 ttimeoutlen=0
set list listchars=tab:\ \ ,trail:·
set ttyfast
set lazyredraw

" remove swap
set noswapfile
set nobackup
set nowb

" set indentation and format options
set shiftwidth=2
set tabstop=2
set smarttab
set expandtab
set smartindent
set formatoptions+=t
set foldmethod=indent
set foldnestmax=3
set nofoldenable
set regexpengine=1

" set scrolling
set scrolloff=8
set sidescrolloff=15
set sidescroll=1

" enable global clipboard
set clipboard=unnamed

" mouse
set mouse=a

" visual improvments
colorscheme railscasts
set colorcolumn=120
xnoremap <leader>p "_dP

" Plugin settings
for fpath in split(globpath('~/.vim/settings', '*.vim'), '\n')
  exe 'source' fpath
endfor

filetype plugin on
filetype indent on

syntax enable
syntax on

" xclip for Linux global clipboard
vmap <C-c> :<Esc>`>a<CR><Esc>mx`<i<CR><Esc>my'xk$v'y!xclip -selection c<CR>u
map <Insert> :set paste<CR>i<CR><CR><Esc>k:.!xclip -o<CR>JxkJx:set nopaste<CR>
let g:tsuquyomi_use_local_typescript = 0
let g:tsuquyomi_use_vimproc = 1
let g:go_fmt_command = "goimports"
